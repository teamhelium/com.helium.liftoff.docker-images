# Liftoff Docker Images
This repository contains a collection of useful Docker images *for local development purposes only*.

## php
The `php` folder contains configurations for processing php applications, including all of the dependencies necessary
to run your web application.

### php fpm
The `php/x.x/fpm` folders contain configurations for serving applications with php-fpm. With Liftoff, use these images
in combination with the latest official `nginx` image to serve applications with different versions of php under the
same NGINX server.